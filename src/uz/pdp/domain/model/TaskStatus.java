package uz.pdp.domain.model;

public enum TaskStatus {
    CREATED,
    ASSIGNED,
    IN_PROGRESS,
    DONE,
    BLOCK,
    CANCELED;
}
