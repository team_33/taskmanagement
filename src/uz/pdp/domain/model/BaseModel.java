package uz.pdp.domain.model;

import java.util.UUID;

public  abstract  class BaseModel {
    {
        this.Id = UUID.randomUUID();
    }
    protected UUID Id;

    public UUID getId() {
        return Id;
    }

    public void setId(UUID Id) {
        this.Id = Id;
    }
}
