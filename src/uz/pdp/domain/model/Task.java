package uz.pdp.domain.model;

import java.util.UUID;

public class Task extends BaseModel{
    private String text;
    private TaskStatus status;
    private UUID creatorId;
    private UUID assigneeId;



    public Task(TaskBuilder taskBuilder, UUID assigneeId) {
        this.status = taskBuilder.status;
        this.text = taskBuilder.text;
        this.creatorId = taskBuilder.creatorId;
        this.assigneeId = assigneeId;
    }

    public static class TaskBuilder{
        private String text;
        private TaskStatus status;
        private UUID creatorId;
        private UUID assigneeId;

        public TaskBuilder setAssigneeId(UUID assigneeId) {
            this.assigneeId = assigneeId;
            return this;
        }

        public TaskBuilder setText(String text) {
            this.text = text;
          return this;
        }

        public TaskBuilder setStatus(TaskStatus status) {
            this.status = status;
          return this;
        }

        public TaskBuilder setCreatorId(UUID creatorId) {
            this.creatorId = creatorId;
            return this;
        }

        public Task build(){
            return new Task(this, assigneeId);
        }

    }

    public UUID getAssigneeId() {
        return assigneeId;
    }

    public void setAssigneeId(UUID assigneeId) {
        this.assigneeId = assigneeId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    public UUID getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(UUID creatorId) {
        this.creatorId = creatorId;
    }
}
