package uz.pdp.domain.controller;

import uz.pdp.domain.DTO.BaseResponse;
import uz.pdp.domain.DTO.SignInDTO;
import uz.pdp.domain.DTO.SignUpDTO;
import uz.pdp.domain.model.User;
import uz.pdp.domain.model.UserRole;
import uz.pdp.domain.util.Util;

import static uz.pdp.domain.controller.BackendDev.backendDevMenu;
import static uz.pdp.domain.controller.BackendLeadUi.backendMenu;
import static uz.pdp.domain.controller.FrontendDev.frontendDevMenu;
import static uz.pdp.domain.controller.FrontendLead.frontendLeadMenu;
import static uz.pdp.domain.controller.ManagerUi.managerMenu;


import static uz.pdp.domain.controller.Tester.testerMenu;
import static uz.pdp.domain.controller.UserUI.userMenu;
import static uz.pdp.domain.model.UserRole.*;
import static uz.pdp.domain.util.Util.*;

public class MainUi {
    public void  runWindow() {
        int a = 0;
        while (true){
            System.out.println("1.Sign up\t2.Sign in\t 0.Exit");
            a = SCANNER_NUM.nextInt();
            switch (a){
                case 1 -> signUP();
                case 2 -> signIN();
                case 0 -> {
                    return;
                }
                default -> System.out.println("Wrong input ");
            }
        }
    }

    private void signUP() {

        System.out.print("Enter name -> ");
        String name = Util.SCANNER_STR.nextLine();

        System.out.print("Enter phoneNumber -> ");
        String phoneNumber = Util.SCANNER_STR.nextLine();

        System.out.print("Enter password -> ");
        String password = Util.SCANNER_STR.nextLine();

        SignUpDTO signUpDTO = new SignUpDTO(name,phoneNumber,password);
        BaseResponse response = userservice.add(signUpDTO);
        if(response.getStatus() == 400){
            System.out.println(" This user is already exists !!! ");
        }else {
            System.out.println("Successfully signed up");
        }
    }

    private void signIN() {
        System.out.print("Enter phone number: ");
        String phoneNumber = SCANNER_STR.nextLine();

        System.out.print("Enter password: ");
        String password = SCANNER_STR.nextLine();
        SignInDTO signInDTO = new SignInDTO(phoneNumber,password);

        BaseResponse<User> response = userservice.singIn(signInDTO);

        if (response.getStatus() == 400){
            System.out.println(response.getMassage());
        }else{
            switch (response.getData().getRole()){
                case MANAGER -> {
                    managerMenu(response.getData());
                }
                case BACKEND_LEAD -> {
                    backendMenu(response.getData());
                }
                case FRONTEND_LEAD -> {
                    frontendLeadMenu(response.getData());
                }
                case BACKEND_DEV -> {
                    backendDevMenu(response.getData());
                }
                case FRONTEND_DEV -> {
                      frontendDevMenu(response.getData());
                }
                case USER ->{
                    userMenu(response.getData());
                }
            }
        }
    }



}
