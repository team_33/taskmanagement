package uz.pdp.domain.controller;

import uz.pdp.domain.model.Task;
import uz.pdp.domain.model.TaskStatus;
import uz.pdp.domain.model.User;
import uz.pdp.domain.model.UserRole;
import uz.pdp.domain.util.Util;

import java.util.ArrayList;

public class Tester {
    public static void testerMenu(User user){
        int a;
        while (true){
            System.out.println("1.Do task\t2.See tasks\t3.Create task for leads\t0.back");
            a = Util.SCANNER_NUM.nextInt();

            switch (a){
                case 1 -> {
                    doTask(user);
                }case 2 -> {
                    seeTasks(user);
                }case 3 -> {
                    createTask(user);
                }case 0 -> {
                    return;
                }
            }
        }

    }

    public static ArrayList<Task> doTask(User user) {
        ArrayList<Task> tasks = unworkedTasks(user);
        System.out.print("Select the index of the task you want to perform -> ");
        int a = Util.SCANNER_NUM.nextInt();
        Task task = tasks.get(a - 1);



    }

    public static ArrayList<Task> seeTasks(User user) {
        ArrayList<Task> tasks = showMyTask(user);
        int i = 1;
        for (Task task : tasks) {
            System.out.println(i++ + " || " + task.getText());
        }
        return tasks;
    }
    public static ArrayList<Task> showMyTask(User user){
        ArrayList<Task> tasks = new ArrayList<>();
        for (Task task : Util.taskrepository.getAll()) {
            if (task.getAssigneeId().equals(user.getId())){
                tasks.add(task);
            }
        }
        return tasks;
    }

    public static void createTask(User user) {
    }
     public static ArrayList<Task> unworkedTasks(User user){
        int k =1;
         for (Task task : Util.taskservice.unworkedTask(user)) {
             System.out.println(k++ + " || "+ task.getText());
         }
         return   Util.taskservice.unworkedTask(user);
     }

}
