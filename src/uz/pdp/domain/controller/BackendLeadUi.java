package uz.pdp.domain.controller;

import uz.pdp.domain.model.Task;
import uz.pdp.domain.model.TaskStatus;
import uz.pdp.domain.model.User;

import static uz.pdp.domain.util.Util.*;

public class BackendLeadUi {
    public static void backendMenu(User user){

        int a = 0;
        while (true){
            System.out.println("1.Create new task \t 2.Task assign \t3.Pass task to another dev" +
                    "\t4.Do task\t5.See task\t0.Back");
            a = SCANNER_NUM.nextInt();
            switch (a){
                case 1 -> createNewTask(user);
                case 2 -> taskAssign(user);
                case 3 -> passTask(user);
                case 4 -> doTask(user);
                case 0 -> {
                    return;
                }
                default -> System.out.println("Wrong input ");
            }
        }

    }

    private static void createNewTask(User user) {
        System.out.println(" Enter task : ");
        String text = SCANNER_STR.nextLine();

        Task task = new  Task.TaskBuilder()
                .setText(text)
                .setCreatorId(user.getId())
                .setStatus(TaskStatus.CREATED)
                .build();
        taskservice.add(task);
        System.out.println(" Task created ");
    }

   public static void taskAssign(User user) {
    }

   public static void passTask(User user) {
    }

   public static void doTask(User user) {
    }
}
