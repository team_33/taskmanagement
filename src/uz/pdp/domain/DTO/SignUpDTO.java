package uz.pdp.domain.DTO;

public record   SignUpDTO(String name, String phoneNumber, String password) {
}
