package uz.pdp.domain.DTO;

public record SignInDTO(String phoneNumber, String password) {
}
