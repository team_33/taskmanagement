package uz.pdp.domain.repository.user;

import uz.pdp.domain.model.Task;
import uz.pdp.domain.model.User;
import uz.pdp.domain.util.Util;

import java.util.ArrayList;
import java.util.UUID;

public class UserRepositoryImpl implements UserRepository {
    private final ArrayList<User> USER_LIST = new ArrayList<>();

    private static final UserRepositoryImpl instance = new UserRepositoryImpl();

    public static UserRepositoryImpl getInstance(){
        return instance;
    }

    @Override
    public int save(User user) {
        USER_LIST.add(user);
        return 1;
    }

    @Override
    public User getById(UUID id) {
        for (User user : USER_LIST) {
            if (user.getId().equals(id)){
                return user;
            }
        }
        return null;
    }

    @Override
    public ArrayList<User> getAll() {
        return USER_LIST;
    }

    @Override
    public void remove(UUID uuid) {
        for (User user : USER_LIST) {
            if (user.getId().equals(uuid)){
                USER_LIST.remove(user);
            }
        }
    }

    @Override
    public void remove(User user) {
        USER_LIST.remove(user);
    }
}
