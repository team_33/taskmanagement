package uz.pdp.domain.repository.task;

import uz.pdp.domain.model.Task;
import uz.pdp.domain.repository.user.UserRepositoryImpl;

import java.util.ArrayList;
import java.util.UUID;

public class TaskRepositoryImpl implements TaskRepository{
    private final ArrayList<Task> TASK_LIST = new ArrayList<>();

    private static final TaskRepositoryImpl instance = new TaskRepositoryImpl();

    public static TaskRepositoryImpl getInstance(){
        return instance;
    }
    @Override
    public int save(Task task) {

        TASK_LIST.add(task);
        return 1;
    }

    @Override
    public Task getById(UUID id) {
        for (Task task : TASK_LIST) {
            if (task.getId().equals(id)) {
                return task;
            }
        }
        return null;
    }

    @Override
    public ArrayList<Task> getAll() {
        return TASK_LIST;
    }

    @Override
    public void remove(UUID uuid) {
        for (Task task : TASK_LIST) {
            if (task.getId().equals(uuid)) {
                TASK_LIST.remove(uuid);
            }
        }
    }

    @Override
    public void remove(Task task) {
        if (TASK_LIST.contains(task)){
            TASK_LIST.remove(task);
        }
    }
}
